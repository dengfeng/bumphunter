#include <iostream>
#include <string>
#include <vector>

#include "TFile.h"
#include "TH1D.h"
#include "atlasrootstyle/AtlasStyle.C"
#include "atlasrootstyle/AtlasLabels.C"
#include "atlasrootstyle/AtlasUtils.C"

using namespace std;

int comSignificance()
{
  SetAtlasStyle() ;

  TFile* inFile_global = new TFile("../results/GenericZX/HVT_global/ZXmass/BumpHunter_MCPlusHVT600_Zee_SR_LeadB_ZXmass.root", "read") ;
  TFile* inFile_highZPt50 = new TFile("../results/GenericZX/HVT_highZPt50/ZXmass/BumpHunter_MCPlusHVT600_Zee_SR_LeadB_ZXmass.root", "read") ;
  TFile* inFile_highZPt100 = new TFile("../results/GenericZX/HVT_highZPt100/ZXmass/BumpHunter_MCPlusHVT600_Zee_SR_LeadB_ZXmass.root", "read") ;

  string Name = string(inFile_global->GetName()) ;
  vector<string> textList ;
  textList.push_back("#sqrt{s} = 13 TeV") ;
  string Category = "" ;
  if(Name.find("LeadB")!=string::npos)
    Category =  "LeadB" ;
  else if(Name.find("LeadB")!=string::npos)
    Category =  "LeadB" ;
  else if(Name.find("LeadB")!=string::npos)
    Category =  "LeadB" ;
  if(Name.find("Zee")!=string::npos)
    textList.push_back("Z#rightarrowe^{+}e^{-} " + Category);
  else if(Name.find("Zee")!=string::npos)
    textList.push_back("Z#rightarrow#mu^{+}#mu^{-} " + Category);

  textList.push_back("SR") ;

  if(!inFile_global || !inFile_highZPt50 || !inFile_highZPt100)
    exit(0) ;

  TH1D* residualHist_global = (TH1D*) inFile_global->Get("residualHist") ;
  TH1D* residualHist_highZPt50 = (TH1D*) inFile_highZPt50->Get("residualHist") ;
  TH1D* residualHist_highZPt100 = (TH1D*) inFile_highZPt100->Get("residualHist") ;
  if(!residualHist_global || !residualHist_highZPt50 || !residualHist_highZPt100)
    exit(0) ;

  residualHist_global->SetTitle("Global") ;
  residualHist_global->SetLineColor(kBlack) ;
  residualHist_global->GetYaxis()->SetTitle("Significance") ;
  residualHist_highZPt50->SetTitle("Z_{p_{T}}>50 GeV") ;
  residualHist_highZPt50->SetLineColor(kBlue) ;
  residualHist_highZPt100->SetTitle("Z_{p_{T}}>100 GeV") ;
  residualHist_highZPt100->SetLineColor(kRed) ;
  
  //ration histogram
  TH1D* residualHist_highZPt50_global = (TH1D*) residualHist_highZPt50->Clone("residualHist_highZPt50_global") ;
  residualHist_highZPt50_global->Divide(residualHist_global) ;
  residualHist_highZPt50_global->SetTitle("#frac{Z_{p_{T}}>50 GeV}{Global}") ;
  residualHist_highZPt50_global->GetYaxis()->SetTitle("Ratio") ;
  residualHist_highZPt50_global->SetLineColor(kBlue) ;
  TH1D* residualHist_highZPt100_global = (TH1D*) residualHist_highZPt100->Clone("residualHist_highZPt100_global") ;
  residualHist_highZPt100_global->Divide(residualHist_global) ;
  residualHist_highZPt100_global->SetTitle("#frac{Z_{p_{T}}>100 GeV}{Global}") ;
  residualHist_highZPt100_global->GetYaxis()->SetTitle("Ratio") ;
  residualHist_highZPt100->SetLineColor(kRed) ;

  // make plots
  TCanvas* c = new TCanvas("c", "", 600, 600) ;
  TPad *pad1 = new TPad("pad1","pad1",0,0.33,1,1) ;
  pad1->SetMargin(0.15, 0.033, 0.02, 0.055) ;
  pad1->SetBorderMode(0) ;
  TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.33) ;
  pad2->SetMargin(0.15, 0.033, 0.35, 0.033) ;
  pad2->SetBorderMode(0) ;

  pad1->Draw() ;
  pad2->Draw() ;

  pad1->cd() ;
  residualHist_global->GetYaxis()->SetRangeUser(0, int(residualHist_highZPt100->GetMaximum()+1)) ;
  residualHist_global->GetYaxis()->SetNdivisions(1005) ;
  residualHist_global->GetXaxis()->SetNdivisions(1005) ;
  residualHist_global->GetXaxis()->SetLabelSize(0.) ;
  residualHist_highZPt50->GetXaxis()->SetLabelSize(0.) ;
  residualHist_highZPt100->GetXaxis()->SetLabelSize(0.) ;
  residualHist_global->Draw("hist")  ;
  residualHist_highZPt50->Draw("hist same")  ;
  residualHist_highZPt100->Draw("hist same")  ;

  ATLASLabel(0.25,0.85,"Internal");
  for(int i=0 ; i<(int)textList.size() ; i++)
    myText(0.25,  0.85-0.07*(i+1), 1, textList.at(i).c_str());

  TLegend* legend_top = new TLegend(0.6,0.6,0.9,0.92);
  legend_top->SetFillStyle(0) ;
  legend_top->SetShadowColor(0) ;
  legend_top->SetBorderSize(0);
  legend_top->SetTextSize(0.04);
  legend_top->AddEntry(residualHist_global,residualHist_global->GetTitle(),"l");
  legend_top->AddEntry(residualHist_highZPt50,residualHist_highZPt50->GetTitle(),"l");
  legend_top->AddEntry(residualHist_highZPt100,residualHist_highZPt100->GetTitle(),"l");
  legend_top->Draw() ;

  pad2->cd() ;
  residualHist_highZPt50_global->GetYaxis()->SetRangeUser(0., 2.) ;
  residualHist_highZPt50_global->GetYaxis()->SetRangeUser(0, int(residualHist_highZPt100_global->GetMaximum()+1)) ;
  residualHist_highZPt50_global->GetXaxis()->SetNdivisions(1005) ;
  residualHist_highZPt50_global->GetXaxis()->SetTitleSize(0.1) ;
  residualHist_highZPt50_global->GetXaxis()->SetLabelSize(0.1) ;
  residualHist_highZPt50_global->GetYaxis()->SetTitleSize(0.1) ;
  residualHist_highZPt50_global->GetYaxis()->SetLabelSize(0.1) ;
  residualHist_highZPt50_global->GetYaxis()->SetTitleOffset(0.7) ;
  residualHist_highZPt50_global->GetYaxis()->SetNdivisions(505) ;
  residualHist_highZPt50_global->Draw("hist")  ;
  residualHist_highZPt100_global->Draw("hist same")  ;

  TLine* line = new TLine (residualHist_highZPt50_global->GetXaxis()->GetXmin(), 1, residualHist_highZPt50_global->GetXaxis()->GetXmax(), 1) ;
  line->SetLineColor(6) ;
  line->SetLineStyle(2) ;
  line->Draw("same") ;

  TLegend* legend_bottom1 = new TLegend(0.6,0.7,0.75,0.95);
  legend_bottom1->SetFillStyle(0) ;
  legend_bottom1->SetShadowColor(0) ;
  legend_bottom1->SetBorderSize(0);
  legend_bottom1->SetTextSize(0.04);
  legend_bottom1->AddEntry(residualHist_highZPt50_global,residualHist_highZPt50_global->GetTitle(),"l");
  legend_bottom1->Draw() ;
  TLegend* legend_bottom2 = new TLegend(0.75,0.7,0.9,0.95);
  legend_bottom2->SetFillStyle(0) ;
  legend_bottom2->SetShadowColor(0) ;
  legend_bottom2->SetBorderSize(0);
  legend_bottom2->SetTextSize(0.04);
  legend_bottom2->AddEntry(residualHist_highZPt100_global,residualHist_highZPt100_global->GetTitle(),"l");
  legend_bottom2->Draw() ;
  c->Print("residualHist_Zee_SR_LeadB_ZXmass.pdf") ;

  return 0 ; 
}
